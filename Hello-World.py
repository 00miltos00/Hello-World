# This is my first Python program, written in PyCharm on Arch Linux for Python 3.6.1.

from math import sqrt, pow
from random import randint

my_name = "Vouzounaras Miltiadis \n"
print(my_name)

sideA = randint(1, 20)
sideB = randint(1, 20)

print("The side A has length of %d and side B has length of %d \n" % (sideA, sideB))

areaA = pow(sideA, 2)
areaB = pow(sideB, 2)
areaC = areaA + areaB
print("The area of the side A is %d and for side B is %d \n" % (areaA, areaB))
print("The area of the side C is %d \n" % (areaC))

sideC = sqrt(areaC)

print("The hypotenuse of the right triangle with sides %d and %d is %f \n" % (sideA, sideB, sideC))

print("All the information for this right triangle: \n side A = %d \n side B = %d \n side C = %f \n area A = %d \n area B = %d \n area C = %d \n" %(sideA, sideB, sideC, areaA, areaB, areaC))
